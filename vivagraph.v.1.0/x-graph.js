/**
 * Created by Core.Today on 2017-05-30.
 */
var graph = Viva.Graph.graph(),
    layout = Viva.Graph.Layout.forceDirected(graph, {
        springLength: 8,
        springCoeff: 0.0001,
        dragCoeff: 0.1,
        gravity: -7
    }),
    container = document.getElementById('render'),
    webglGraphics = Viva.Graph.View.webglGraphics(),
    events = Viva.Graph.webglInputEvents(webglGraphics, graph),
    renderer = Viva.Graph.View.renderer(graph, {
        layout: layout,
        graphics: webglGraphics,
        renderLinks: true,
        prerender: true,
        container: container
    }),
    cur = $('#current');
graph.labels = Object.create(null);
graph.nodesUIArray = Array();

var domain = "http://52.79.177.143:8081/g/";
var primaryNodeId = $('.badge-info')[0].innerText;


function xposition(ui) {
    if (Math.abs(ui.node.xPosition) > 0 && Math.abs(ui.node.xPosition) <= 10) {
        return ui.node.xPosition * 40
    } else if (Math.abs(ui.node.xPosition) > 10 && Math.abs(ui.node.xPosition) <= 20) {
        return ui.node.xPosition * 40
    } else if (Math.abs(ui.node.xPosition) > 20 && Math.abs(ui.node.xPosition) <= 30) {
        return ui.node.xPosition * 40
    } else {
        return ui.node.xPosition
    }
}


webglGraphics.placeNode(function (ui, pos) {
// ui는 color, id(임의로 정해진 0부터 n까지의 수), 그리고 addNode할 때 넣어준 node 정보와 position 객체가 들어있음.
// position 객체는 위에서 넣어준 두번째 parameter 인 pos 로 꺼내 올 수도 있음. pos 는 위의 ui 에서 position 객체만 분리
// color나 size를 바꾸려면 ui로, position을 바꾸려면 pos로 쓰는게 편리

    if (ui.node.data.y !== "") {
        if (ui.node.xPosition !== undefined) {
            ui.position.x = xposition(ui)
            /*(ui.node.xPosition)*30;*/
        }
    }

    var domPos = {
        x: pos.x,
        y: pos.y
    };
    webglGraphics.transformGraphToClientCoordinates(domPos);

    var nodeId = ui.node.id;
    if (graph.labels[nodeId] !== undefined & graph.getNode(nodeId).data !== undefined) {
        var labelStyle = graph.labels[nodeId].style;
        labelStyle.left = domPos.x + 'px';
        labelStyle.top = domPos.y + 20 + 'px';
        graph.labels[nodeId].innerText = graph.getNode(nodeId).data.n;
        container.appendChild(graph.labels[nodeId])
    } else {
        labelStyle = null
    }
    ;
});


function addLabel(node) {
    if (typeof(node.id) == "number") {
        node.id = node.id;
    }
    if (Object.keys(graph.labels).indexOf(node.id) !== -1) {
        return null;
    } else {
        graph.labels[node.id] = document.createElement('label');
        graph.labels[node.id].classList.add('node-label');
    }
};

events.click(function (node) {
    console.log('click');
    // console.log(node);
}).dblClick(function (node) {
    console.log('dblclick');
    console.log(webglGraphics.getNodeUI(node.id));
    addnodes(node.data.n);
    addDblClickNodes(node)
});

function addnodes(node) {
    $('#g-loading').css('visibility', 'visible');
    var url = domain + node
    $.getJSON(url, function (data) {
        $('#g-loading').css('visibility', 'hidden');
        data.node.isPinned = true;
        graph.addNode(data.node.i, data.node);
        edgesDraw(data)
        linksDraw(data)
        insertDataForEachNode(data)
    }); //getJSON() 끝 줄
}; //addnodes() 끝 줄

function addDblClickNodes(node) {
    var nodeUIid = webglGraphics.getNodeUI(node.id).id
    console.log(nodeUIid)
    if (graph.nodesUIArray.indexOf(nodeUIid) !== -1) {
        graph.nodesUIArray.push(nodeUIid)
        console.log(graph.nodesUIArray)
    }
}

function edgesDraw(data) {
    for (var i = 0; i < data.nodes.length; ++i) {
        var edge = data.edges[i];
        graph.addLink(edge.s, edge.t);
    }
};
function linksDraw(data) {
    for (var i = 0; i < data.edges.length; ++i) {
        var link = data.edges[i];
        graph.addLink(link.s, link.t)
    }
};
function fillColor(node) {
    var nodeUI = webglGraphics.getNodeUI(node.id);
    if (node.data.k !== undefined) {
        if (node.data.k === 'j') {
            nodeUI.size = 20;
            nodeUI.color = 0xFF0048FF;
        } else if (node.data.k === 'p') {
            nodeUI.size = 20;
            nodeUI.color = 0x009DFFFF;
        }
    } else {
        nodeUI.size = 10;
        nodeUI.color = 0x1c1c1c88;
    }
}
function insertDataForEachNode(data) {
    var nodesData = data.nodes,
        nodeData = data.node;

    graph.forEachNode(function (node) {
        var nodeUI = webglGraphics.getNodeUI(node.id);
        console.log(graph.nodesUIArray.push(nodeUI))
        console.log(graph.nodesUIArray)
        // if(graph.nodesUIArray[node].id.toString().indexOf(nodeUI.id) !== -1) {
        //     graph.nodesUIArray.push(nodeUI)
        // }

        // console.log(nodeUI)
        // console.log(nodeData)
        for (var i = 0; i < nodesData.length; ++i) {
            if (node.id === nodesData[i].i) {
                graph.getNode(node.id).data = nodesData[i];
                // console.log(nodesData[i])
                if (node.data.y !== "" && node.xPosition !== 0) {
                    node.xPosition = Object;
                    node.xPosition = {};
                    node.xPosition = node.data.y - data.node.y
                    console.log(node.data.y + "-" + data.node.y + "=" + node.xPosition)
                }
            } else if (node.id === nodeData.i && nodeUI.node.xPosition === undefined) {
                node.xPosition = Object;
                node.xPosition = {};
                node.xPosition = 0
            }
        }
        fillColor(node)
        if (node.data.n !== undefined) {
            addLabel(node)
        }
    }); // forEachNode() 끝 줄
}
function dblClickInsertData(data) {
    var nodesData = data.nodes,
        nodeData = data.node;

    graph.forEachNode(function (node) {
        console.log(node)
        var nodeUI = webglGraphics.getNodeUI(node.id);
        for (var i = 0; i < nodesData.length; ++i) {
            if (node.id === nodesData[i].i) {
                graph.getNode(node.id).data = nodesData[i];
                // console.log(nodesData[i])
                if (node.data.y !== "" && node.xPosition !== 0) {
                    node.xPosition = Object;
                    node.xPosition = {};
                    node.xPosition = node.data.y - data.node.y
                    console.log(node.data.y + "-" + data.node.y + "=" + node.xPosition)
                }
            } else if (node.id === nodeData.i && nodeUI.node.xPosition === undefined) {
                node.xPosition = Object;
                node.xPosition = {};
                node.xPosition = 0
            }
        }
        fillColor(node)
        if (node.data.n !== undefined) {
            addLabel(node)
        }
    }); // forEachNode() 끝 줄
}


renderer.run()
addnodes($('.badge-info')[0].innerText)


$(document).ready(function () {
        console.log('ready')

    }
)
$('#render').append('<div style="position: absolute;bottom: 40px;border-bottom: solid 1px;width: 520px;left: 40px;"></div>')